# Forms
## initFormTools(context, { Input })
```javascript
class Form extends React.Component {
    constructor(props) {
        super(props)
        initFormTools(this, { Input: SomeInputLikeComponent })

        this.Name = this.createInput('name', {
            value: 'Default value',
            transform: [toUpper, trim],
            validate: [
                [isNotEmpty, 'Required'],
                [isNotVolanDeMort, 'Tsss...']
            ]
        })
    }

    render() {
        const { Name, submit, getHint, clearHint } = this

        //submit() calls this.props.onSubmit()
        return pug`
            form(onSubmit=submit)
                Name(onChange=clearHint('name'))
                span.hint ${getHint('name')}
                submit Submit
        `
    }
}
```
