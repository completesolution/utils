export default (target, key, descriptor) => ({
	configurable: true,
	get() {
		const bound = descriptor.value.bind(this)

		Object.defineProperty(this, key, {
			value: bound,
			configurable: true,
			writable: true
		})

		return bound
	}
})
