import { mapValues, isArray, flow } from 'lodash'
import { pickBy, mapValues as map, nth } from 'lodash/fp'

export default data => target => {
	target.propTypes = mapValues(data, value =>
		isArray(value) ? value[0] : value)

	target.defaultProps = flow(
		pickBy(isArray),
		map(nth(1))
	)(data)

	return target
}
