import props from '../props'
import PropTypes from 'prop-types'

describe('@props', () => {

	it('should define props in class', () => {

		@props({
			name: PropTypes.string.isRequired,
			age: [PropTypes.number, 18]
		})
		class A {
			constructor() { /* ... */ }
		}

		expect(A).toHaveProperty('propTypes', {
			name: PropTypes.string.isRequired,
			age: PropTypes.number
		})

		expect(A).toHaveProperty('defaultProps', {
			age: 18
		})
	})

})
