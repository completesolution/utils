import bind from '../bind'

describe('@bind', () => {

	it('should bind method', () => {

		class A {
			constructor() { /* ... */ }

			@bind
			method() {
				this.value = true
			}
		}

		const a = new A
		const { method } = a

		expect(a).not.toHaveProperty('value')
		method()
		expect(a).toHaveProperty('value', true)

	})

})
