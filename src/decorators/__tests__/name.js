import name from '../name'

describe('@name', () => {

	it('should name class', () => {

		@name('A')
		class A {
			constructor() { /* ... */ }
		}

		expect(A).toHaveProperty('displayName', 'A')
	})
	
})
