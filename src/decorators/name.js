export default displayName => target => {
	target.displayName = displayName
	return target
}
