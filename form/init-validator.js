import { isArray, find, map, isUndefined } from 'lodash'

function createValidator([predicate, hint]) {
	return value => predicate(value) ? '' : hint
}

function createMultipleValidator(config) {
	const validators = map(config, createValidator)

	return value => {
		const hints = map(validators, validator => validator(value))
		const hint = find(hints, hint => hint !== '')
		return hint ? hint : ''
	}
}

function getValidatorCreator(config) {
	const creator = isArray(config[0]) ?
		createMultipleValidator :
		createValidator

	return () => creator(config)
}

export default function initValidator(config, getField, setHint) {
	const validator = getValidatorCreator(config)()

	return () => {
		let hint
		setHint(hint = validator(getField()))
		// console.log('hint', hint);
		return hint === ''
	}
}
