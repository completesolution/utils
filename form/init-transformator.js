import { flow, isArray } from 'lodash'

export default transform =>
	isArray(transform) ? flow(...transform) : transform
