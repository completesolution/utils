import { mapValues, every, each } from 'lodash'

export const getField = context => name =>
	context.state.fields[name] || ''

export const setField = context => name => value =>
	context.setState({
		fields: {
			...context.state.fields,
			[name]: value
		}
	})

export const clearField = context => name => () =>
	context.setState({
		fields: {
			...context.state.fields,
			[name]: ''
		}
	})

export const clearFields = context => () =>
	context.setState({
		fields: mapValues(context.state.fields, () => '')
	})

export const getHint = context => name =>
	context.state.hints[name] || ''

export const setHint = context => {
	const defered = new Defered(context, { hints: {} })

	return name => value => {
		defered.setState({
			hints: {
				...defered.state.hints,
				[name]: value
			}
		})
	}
}

export const clearHint = context => name => () =>
	context.setState({
		hints: {
			...context.state.hints,
			[name]: ''
		}
	})

export const clearHints = context => () =>
	context.setState({
		hints: mapValues(context.state.hints, () => '')
	})

export const isNoHints = context => () => {
	return every(context.state.hints, hint => hint === '')
}

export const addValidator = context => validator =>
	context.validators.push(validator)

export const submit = context => () => {
	if (context.validate()) {
		context.props.onSubmit({
			...context.state.fields
		})
	}
}

class Defered {
	constructor(context, defaultState = {}) {
		this.context = context
		this.defaultState = defaultState
		this.state = defaultState
		this.timeout = false

		this.setState = this.setState.bind(this)
		this.defer = this.defer.bind(this)
		this.deferedSet = this.deferedSet.bind(this)
	}

	setState(state) {
		this.state = {
			...this.state,
			...state
		}

		this.defer()
	}

	defer() {
		if (!this.timeout) {
			this.timeout = setTimeout(this.deferedSet, 0)
		}
	}

	deferedSet() {
		this.context.setState(this.state)
		this.state = { ...this.defaultState }
		this.timeout = false
	}
}
