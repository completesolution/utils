import { flow, capitalize, toNumber } from 'lodash'
import { split, map, join } from 'lodash/fp'

export const capitalizeAllWords = flow(
	split(' '),
	map(capitalize),
	join(' ')
)

export const limit = (min, max = Infinity) => value =>
	value < min ? min : value > max ? max : value

export { toNumber }
