import { identity, constant } from 'lodash'
import initTransformator from './init-transformator'
import initValidator from './init-validator'

export default (context, Component) => {
	return (name, {
		value = '',
		transform = identity,
		validate = [constant(true), '']
	}) => {
		const getField = () => context.getField(name)
		const setField = context.setField(name)
		// const getHint = () => context.getHint(name)
		const setHint = context.setHint(name)

		transform = initTransformator(transform)
		validate = initValidator(validate, getField, setHint)

		context.addValidator(name, validate)

		const handleChange = props => e => {
			const { value } = e.target
			setField(transform(value))
			props.onChange(e)
		}

		return props => pug`
			Component(
				...props
				name=name
				onChange=handleChange(props)
				value=getField()
			)
		`
	}
}
