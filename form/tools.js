import { reduce, each } from 'lodash'
import initInputCreator from './input-creator'
import initSelectCreator from './select-creator'
import * as actions from './actions'

const bindAction = context => (action, name) =>
	context[name] = action(context)

const defineFormActions = context =>
	each(actions, bindAction(context))

const DefaultInput = props => pug`input(...props)`
const DefaultSelect = props => pug`select(...props)`

export default function initFormTools(context, {
	Input = DefaultInput,
	Select = DefaultSelect
}) {

	defineFormActions(context)

	context.state = {
		...context.state,
		fields: {},
		hints: {}
	}

	context.validators = []
	context.validate = () =>
		reduce(
			context.validators,
			(result, validator) => validator() && result,
			true
		)

	context.createInput = initInputCreator(context, Input)
	context.createSelect = initSelectCreator(context, Select)
}
