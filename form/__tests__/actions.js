import { each } from 'lodash'
import * as actions from '../actions'

const bindAction = context => (action, name) => {
	// console.log(action.toString());
	// console.log(context);
	context[name] = action(context)
}

const defineFormActions = context =>
	each(actions, bindAction(context))

class ComponentMock {
	constructor() {
		defineFormActions(this)

		this.state = {
			fields: {},
			hints: {}
		}

		this.validators = {}
		this.validate = () =>
			each(this.validators, validator => validator())
	}

	setState(state) {
		this.state = {
			...this.state,
			...state
		}
	}
}

let component

beforeEach(() => {
	component = new ComponentMock
})

describe('Field', () => {

	it('should set field', () => {
		component.setField('name')('Askar')

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ fields: { name: 'Askar' } })
		}))
	})

	it('should get field', () => {
		component.setField('name')('Askar')
		const name = component.getField('name')

		expect(name).toEqual('Askar')
	})

	it('should clear field', () => {
		component.setField('name')('Askar')
		component.clearField('name')()
		const name = component.getField('name')

		expect(name).toEqual('')
	})

	it('should set many fields', () => {
		component.setField('name')('Askar')
		component.setField('status')('online')
		component.setField('age')(25)

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ fields: {
				name: 'Askar',
				status: 'online',
				age: 25
			} })
		}))
	})

	it('should clear many fields', () => {
		component.setField('name')('Askar')
		component.setField('status')('online')
		component.setField('age')(25)

		component.clearFields()

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ fields: {
				name: '',
				status: '',
				age: ''
			} })
		}))
	})

})

/* async hints ***
describe('Hint', () => {
	it('should set hint', () => {
		component.setHint('name')('Askar')

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ hints: { name: 'Askar' } })
		}))
	})

	it('should get hint', () => {
		component.setHint('name')('Askar')
		const name = component.getHint('name')

		expect(name).toEqual('Askar')
	})

	it('should clear hint', () => {
		component.setHint('name')('Askar')
		component.clearHint('name')
		const name = component.getHint('name')

		expect(name).toEqual('')
	})

	it('should set many hints', () => {
		component.setHint('name')('Askar')
		component.setHint('status')('online')
		component.setHint('age')(25)

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ hints: {
				name: 'Askar',
				status: 'online',
				age: 25
			} })
		}))
	})

	it('should clear many hints', () => {
		component.setHint('name')('Askar')
		component.setHint('status')('online')
		component.setHint('age')(25)

		component.clearHints()

		expect(component).toEqual(expect.objectContaining({
			state: expect.objectContaining({ hints: {
				name: '',
				status: '',
				age: ''
			} })
		}))
	})

	it('should check that hints empty', () => {
		component.setHint('name')('Askar')
		component.setHint('status')('online')
		component.setHint('age')(25)

		expect(component.isNoHints()).toBeFalsy()

		component.clearHints()

		expect(component.isNoHints()).toBeTruthy()
	})

	// it('should submit', () => {
	// 	component.setField('name')('Askar')
	// 	component.setField('status')('online')
	// 	component.setField('age')(25)
	//
	// 	component.addValidator()
	// 	component.validators = {
	// 		name: (value) =>
	// 			component.setHint(value.length < 10 ? '' : 'Length Error'),
	// 		age: (value) =>
	// 			component.setHint(value > 17 ? '' : 'Age Error')
	// 	}
	//
	// 	component.props = {
	// 		onSubmit: jest.fn()
	// 	}
	//
	// 	component.submit()
	//
	// 	expect(component.props.onSubmit.mock.calls[0][0]).toEqual(
	// 		expect.objectContaining({
	// 			name: 'Askar',
	// 			status: 'online',
	// 			age: 25
	// 		})
	// 	)
	//
	// 	component.setField('name')('Askar Mukhametshin')
	// 	component.submit()
	//
	// 	expect(component.props.onSubmit.mock.calls.length).toBe(1)
	// 	expect(component.isNoHints()).toBeFalsy()
	// })
})
*** */
