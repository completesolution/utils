import initTransformator from '../init-transformator'

describe('initTransformator', () => {

	it('should init single transformer', () => {
		const A = jest.fn()

		A.mockReturnValue('A')

		const transformer = initTransformator(A)
		const result = transformer('X')

		expect(A).toBeCalledWith('X')
		expect(result).toBe('A')
	})

	it('should init multiple transformers', () => {
		const A = jest.fn()
		const B = jest.fn()
		const C = jest.fn()

		A.mockReturnValue('A')
		B.mockReturnValue('B')
		C.mockReturnValue('C')

		const transformer = initTransformator([A, B, C])
		const result = transformer('X')

		expect(A).toBeCalledWith('X')
		expect(B).toBeCalledWith('A')
		expect(C).toBeCalledWith('B')
		expect(result).toBe('C')
	})

})
