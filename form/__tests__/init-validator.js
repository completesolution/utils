import initValidator from '../init-validator'

describe('initValidator', () => {

	it('should validate single hint', () => {
		const getField = jest.fn()
		const setHint = jest.fn()

		getField
			.mockReturnValueOnce('XXX')
			.mockReturnValueOnce('YYY')

		const validator = initValidator(
			[value => value === 'XXX', 'Not XXX'],
			getField,
			setHint
		)

		validator()

		expect(getField.mock.calls.length).toBe(1)
		expect(setHint).toBeCalledWith('')

		validator()

		expect(getField.mock.calls.length).toBe(2)
		expect(setHint).toBeCalledWith('Not XXX')
	})

	it('should validate multiple hint', () => {
		const getField = jest.fn()
		const setHint = jest.fn()

		getField
			.mockReturnValueOnce('XXXY')
			.mockReturnValueOnce('XXY')
			.mockReturnValueOnce('XXZ')

		const validator = initValidator(
			[
				[value => value.startsWith('XX'), 'Not stats with XX'],
				[value => value.endsWith('Y'), 'Not ends with Y']
			],
			getField,
			setHint
		)

		validator()

		expect(getField.mock.calls.length).toBe(1)
		expect(setHint).toBeCalledWith('')

		validator()

		expect(getField.mock.calls.length).toBe(2)
		expect(setHint).toBeCalledWith('')

		validator()

		expect(getField.mock.calls.length).toBe(3)
		expect(setHint).toBeCalledWith('Not ends with Y')
	})

})
