export const isNotEmpty = value => value !== ''
export const isEquals = constant => value => value === constant
export const isMoreThan = constant => value => value > constant
export const isLessThan = constant => value => value < constant
